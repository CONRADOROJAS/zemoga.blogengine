﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Zemoga.BlogEngine.Core.Enums;

namespace Zemoga.BlogEngine.API.Authorization
{
    public static class AuthorizationPolicyBuilderExtensions
    {
        public static AuthorizationPolicyBuilder RequireRole(this AuthorizationPolicyBuilder policy, Role role)
        {
            return policy.RequireClaim("Role", role.ToString());
        }
    }
}
