﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Zemoga.BlogEngine.Core.Models;

namespace Zemoga.BlogEngine.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _config;

        public AuthController(IConfiguration config)
        {
            _config = config;
        }

        [HttpPost]
        [Route("/Auth/Login")]
        public async Task<IActionResult> Login([FromBody] LoginModel loginModel)
        {
            var tokenMessage = new TokenMessage();
            if (string.IsNullOrEmpty(loginModel.UserName) || string.IsNullOrEmpty(loginModel.Password))
            {
                return BadRequest();
            }
            UserData userData = new();
            User? user = userData.Users.FirstOrDefault(x => x.UserName == loginModel.UserName && x.Password == loginModel.Password);
            if (user == null || user.IdUser == 0)
            {
                return Ok(tokenMessage);
            }
            tokenMessage.User = user;
            // Create claims for the user
            var claims = new[]
            {
                new Claim(ClaimTypes.Role, user.Role)
                // Add any additional claims as required
            };

            // Create a symmetric security key using the secret key stored in the appsettings.json file
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JWTSettings:SecretKey"]));

            // Generate a token
            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.UtcNow.AddDays(1),
                signingCredentials: new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
            );
            tokenMessage.Token = new JwtSecurityTokenHandler().WriteToken(token);
            // Return the token as a JSON response
            return Ok(tokenMessage);
        }
    }
}
