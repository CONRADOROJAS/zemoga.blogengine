using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Zemoga.BlogEngine.Business.Interfaces;
using Zemoga.BlogEngine.Core.EF;
using Zemoga.BlogEngine.Core.Models;

namespace Zemoga.BlogEngine.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    public class BlogEngineController : ControllerBase
    {

        private readonly ILogger<BlogEngineController> _logger;
        private readonly IBlogEngineService _BlogEngineService;

        public BlogEngineController(ILogger<BlogEngineController> logger, IBlogEngineService blogEngineService)
        {
            _logger = logger;
            _BlogEngineService = blogEngineService;
        }

        [HttpGet]
        [Route("/BlogEngine/GetPostById/{idPost}")]
        [AllowAnonymous]
        public BlogPost GetPostById(long idPost)
        {
            try
            {
                return _BlogEngineService.GetPost(x => x.IdPost == idPost) ?? new BlogPost();
            }
            catch (Exception ex)
            {
                _logger.LogError("Error: {Message}", ex.Message);
                throw;
            }
            
        }

        [HttpGet]
        [Route("/BlogEngine/GetAllPost")]
        [AllowAnonymous]
        public List<BlogPost> GetAllPost()
        {
            try
            {
                return _BlogEngineService.GetAllPost();
            }
            catch (Exception ex)
            {
                _logger.LogError("Error: {Message}", ex.Message);
                throw;
            }
        }

        [HttpGet]
        [Route("/BlogEngine/GetPostsByAuthor/{author}")]
        [AllowAnonymous]
        public List<BlogPost> GetPostsByAuthor(string author)
        {
            try
            {
                return _BlogEngineService.GetPosts(x => x.Author == author);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error: {Message}", ex.Message);
                throw;
            }
        }

        [HttpGet]
        [Route("/BlogEngine/GetPostsByStatus/{status}")]
        [AllowAnonymous]
        public List<BlogPost> GetPostsByStatus(string status)
        {
            try
            {
                return _BlogEngineService.GetPosts(x => x.Status == status);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error: {Message}", ex.Message);
                throw;
            }
        }

        [HttpGet]
        [Route("/BlogEngine/GetPostWithComments/{idPost}")]
        [AllowAnonymous]
        public PostDetail GetPostWithComments(long idPost)
        {
            return _BlogEngineService.GetPostWithComments(idPost);
        }


        [HttpPost]
        [Route("/BlogEngine/CreatePost")]
        [Authorize(Policy = "Writer")]
        public BlogPost CreatePost(BlogPost post)
        {
            try
            {
                return _BlogEngineService.CreatePost(post);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error: {Message}", ex.Message);
                throw;
            }
        }

        [HttpPost]
        [Route("/BlogEngine/UpdatePost")]
        [Authorize(Policy = "Writer")]
        public BlogPost UpdatePost(BlogPost post)
        {
            try
            {
                return _BlogEngineService.UpdatePost(post);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error: {Message}", ex.Message);
                throw;
            }
        }

        [HttpPost]
        [Route("/BlogEngine/SubmitPost")]
        [Authorize(Policy = "Writer")]
        public BlogPost SubmitPost(BlogPost post)
        {
            try
            {
                return _BlogEngineService.SubmitPost(post);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error: {Message}", ex.Message);
                throw;
            }
        }

        [HttpPost]
        [Route("/BlogEngine/PublishPost")]
        [Authorize(Policy = "Editor")]
        public BlogPost PublishPost(BlogPost post)
        {
            try
            {
                return _BlogEngineService.PublishPost(post);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error: {Message}", ex.Message);
                throw;
            }
        }

        [HttpPost]
        [Route("/BlogEngine/RejectPost")]
        [Authorize(Policy = "Editor")]
        public BlogPost RejectPost(BlogPost post)
        {
            try
            {
                return _BlogEngineService.RejectPost(post);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error: {Message}", ex.Message);
                throw;
            }
        }

        [HttpPost]
        [Route("/BlogEngine/AddCommentPost")]
        [AllowAnonymous]
        public BlogComment AddCommentPost(BlogComment blogComment)
        {
            try
            {
                return _BlogEngineService.AddCommentPost(blogComment);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error: {Message}", ex.Message);
                throw;
            }
        }

    }
}