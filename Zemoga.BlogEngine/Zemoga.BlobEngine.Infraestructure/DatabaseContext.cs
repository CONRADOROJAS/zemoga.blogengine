﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zemoga.BlogEngine.Core.EF;

namespace Zemoga.BlogEngine.Infraestructure
{
    public class DatabaseContext : DbContext
    {
        public DbSet<BlogPost> BlogPost { get; set; }
        public DbSet<BlogComment> BlogComment { get; set; }


        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<BlogPost>(eb =>
            {
                eb.HasKey("IdPost");
            });
            builder.Entity<BlogComment>(eb =>
            {
                eb.HasKey("IdComment");
            });
        }
    }
}
