﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Zemoga.BlogEngine.Infraestructure.Interfaces;

namespace Zemoga.BlogEngine.Infraestructure.Implementations
{
    public class RepositoryBlogEngine<T> : IRepositoryBlogEngine<T> where T : class
    {
        protected readonly DatabaseContext context;
        private readonly DbSet<T> entities;

        public RepositoryBlogEngine(DatabaseContext context)
        {
            this.context = context;
            entities = context.Set<T>();
        }
        public IEnumerable<T> GetAll()
        {
            return entities.AsEnumerable();
        }

        public IEnumerable<T> Get(Expression<Func<T, bool>> predicate)
        {
            return entities.Where(predicate).AsEnumerable();
        }

        public T Insert(T entity)
        {
            entities.Add(entity);
            context.SaveChanges();
            return entity;
        }
        public T Update(T entity)
        {
            entities.Update(entity);
            context.SaveChanges();
            return entity;
        }

        public T Delete(T entity)
        {
            entities.Remove(entity);
            context.SaveChanges();
            return entity;
        }
    }
}
