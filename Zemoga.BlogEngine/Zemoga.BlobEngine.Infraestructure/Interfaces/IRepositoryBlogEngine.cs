﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Zemoga.BlogEngine.Infraestructure.Interfaces
{
    public interface IRepositoryBlogEngine<T> where T : class
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> Get(Expression<Func<T, bool>> predicate);

        T Insert(T entity);
        T Update(T entity);
        T Delete(T entity);

    }
}
