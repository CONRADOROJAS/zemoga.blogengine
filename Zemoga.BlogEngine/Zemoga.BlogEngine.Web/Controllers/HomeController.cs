﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using Zemoga.BlogEngine.Web.Models;
using Zemoga.BlogEngine.Web.Services.Interfaces;
using Zemoga.BlogEngine.Web.Utilidades;

namespace Zemoga.BlogEngine.Web.Controllers
{
    public class HomeController : Controller
    {

        public HomeController()
        {
        }

        public async Task<IActionResult> Index()
        {
            return RedirectToAction("GetPosts", "BlogEngine");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}