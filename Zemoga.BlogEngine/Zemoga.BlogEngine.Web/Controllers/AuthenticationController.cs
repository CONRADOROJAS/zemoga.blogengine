﻿using Microsoft.AspNetCore.Mvc;
using Zemoga.BlogEngine.Web.Models;
using Zemoga.BlogEngine.Web.Services.Interfaces;
using Zemoga.BlogEngine.Web.Utilidades;

namespace Zemoga.BlogEngine.Web.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly IAuthenticationServices _AuthenticationServices;

        public AuthenticationController(IAuthenticationServices authenticationServices)
        {
            _AuthenticationServices = authenticationServices;
        }

        public IActionResult Login()
        {
            return View();
        }


        [HttpPost]
        public async Task<ActionResult> Login(LoginModelVM model)
        {
            ViewBag.Error = string.Empty;
            TokenMessage tokenMessage = await _AuthenticationServices.Login(model);
            if (string.IsNullOrEmpty(tokenMessage.Token))
            {
                ViewBag.Error =  "User Not Found";
                return View();
            }
            else
            {
                HttpContext.Session.SetComplexData("TokenMessage", tokenMessage);
            }

            return RedirectToAction("GetPosts", "BlogEngine");

        }

        public IActionResult Logout()
        {
            HttpContext.Session.SetComplexData("TokenMessage", new TokenMessage());
            return RedirectToAction("GetPosts", "BlogEngine");
        }
    }
}
