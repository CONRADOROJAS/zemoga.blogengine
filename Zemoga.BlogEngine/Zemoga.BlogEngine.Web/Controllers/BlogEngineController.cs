﻿using Microsoft.AspNetCore.Mvc;
using Zemoga.BlogEngine.Web.Models;
using Zemoga.BlogEngine.Web.Services.Interfaces;
using Zemoga.BlogEngine.Web.Utilidades;

namespace Zemoga.BlogEngine.Web.Controllers
{
    public class BlogEngineController : Controller
    {

        private readonly IBlogEngineService _BlogEngineService;
        private User UserAuthenticated
        {
            get
            {
                var tokenMessage = HttpContext.Session.GetComplexData<TokenMessage>("TokenMessage");
                return tokenMessage != null && tokenMessage.User.IdUser > 0 ? tokenMessage.User : new User();
            }
            set { }
        }

        public BlogEngineController(IBlogEngineService blogEngineService)
        {
            _BlogEngineService = blogEngineService;
        }

        public IActionResult GetPosts()
        {
            ViewBag.Name = string.Empty;
            var listPosts = UserAuthenticated.Role switch
            {
                Role.Writer => _BlogEngineService.GetPostsByAuthor(UserAuthenticated.UserName),
                Role.Editor => _BlogEngineService.GetPostsByStatus(StatusPost.PendingAproval),
                _ => _BlogEngineService.GetPostsByStatus(StatusPost.Published),
            };
            ViewBag.User = UserAuthenticated;
            return View(listPosts);
        }

        public IActionResult DetailPost(long idPost)
        {
            ViewBag.User = UserAuthenticated;
            var postDetails = _BlogEngineService.GetPostWithComments(idPost);
            ViewBag.CountComments = postDetails.BlogComments.Count;
            return View(postDetails);
        }

        [HttpPost]
        public async Task<IActionResult> DetailPost(BlogComment model)
        {
            model.DateComment = System.DateTime.Now;
            await _BlogEngineService.AddCommentPost(model);
            return RedirectToAction("DetailPost", new { idPost = model .IdPost});
        }

        [HttpPost]
        public async Task<IActionResult> CreatePost(BlogPost model)
        {
            model.PublishedDate = System.DateTime.Now;
            model.Status = "Created";
            await _BlogEngineService.CreatePost(model);
            return RedirectToAction("GetPosts");
        }

        [HttpPost]
        public async Task<IActionResult> EditPost(BlogPost model)
        {
            model.PublishedDate = System.DateTime.Now;
            model.Status = "Edited";
            await _BlogEngineService.UpdatePost(model);
            return RedirectToAction("GetPosts");
        }

        [HttpPost]
        public async Task<IActionResult> SubmitPost(long idPost)
        {
            var post = _BlogEngineService.GetPostById(idPost);
            await _BlogEngineService.SubmitPost(post);
            return RedirectToAction("GetPosts");
        }

        [HttpPost]
        public async Task<IActionResult> PublishPost(long idPost)
        {
            var post = _BlogEngineService.GetPostById(idPost);
            await _BlogEngineService.PublishPost(post);
            return RedirectToAction("GetPosts");
        }

        [HttpPost]
        public async Task<IActionResult> RejectPost(long idPost, string comment)
        {
            var post = _BlogEngineService.GetPostById(idPost);
            await _BlogEngineService.RejectPost(post);

            var blogComment = new BlogComment
            {
                DateComment = System.DateTime.Now,
                Comment = comment,
                IdPost = idPost,
                Author = UserAuthenticated.UserName
            };
            await _BlogEngineService.AddCommentPost(blogComment);
            return RedirectToAction("GetPosts");
        }
    }
}
