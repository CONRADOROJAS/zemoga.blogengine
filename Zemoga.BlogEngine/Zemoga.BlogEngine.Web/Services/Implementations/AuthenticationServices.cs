﻿using System.Text.Json;
using Zemoga.BlogEngine.Web.Models;
using Zemoga.BlogEngine.Web.Services.Interfaces;

namespace Zemoga.BlogEngine.Web.Services.Implementations
{
    public class AuthenticationServices : IAuthenticationServices
    {

        private readonly IConfiguration _Configuration;
        private readonly HttpClient _HttpClient;
        private readonly string _baseUrl = string.Empty;

        public AuthenticationServices(IConfiguration configuration, HttpClient httpClient)
        {
            _Configuration = configuration;
            _HttpClient = httpClient;
            _baseUrl = this._Configuration.GetSection("BlogEngineAPI").GetValue<string>("BasePath");
        }

        public async Task<TokenMessage> Login(LoginModelVM loginModelVM)
        {
            var url = $"{_baseUrl}/Auth/Login";
            var json = JsonSerializer.Serialize(loginModelVM);
            var result = await _HttpClient.PostAsync(url, new StringContent(json, System.Text.Encoding.UTF8, "application/json"));

            var tokenMessage = JsonSerializer.DeserializeAsync<TokenMessage>(result.Content.ReadAsStreamAsync().Result, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }).Result;
            return tokenMessage;
        }

    }
}
