﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text.Json;
using Zemoga.BlogEngine.Web.Models;
using Zemoga.BlogEngine.Web.Services.Interfaces;
using Zemoga.BlogEngine.Web.Utilidades;

namespace Zemoga.BlogEngine.Web.Services.Implementations
{
    public class BlogEngineService : IBlogEngineService
    {

        private readonly IConfiguration _Configuration;
        private readonly HttpClient _HttpClient;
        private readonly string _baseUrl = string.Empty;
        private readonly IHttpContextAccessor _httpContextAccessor;

        private string Token
        {
            get
            {
                var tokenMessage = _httpContextAccessor.HttpContext.Session.GetComplexData<TokenMessage>("TokenMessage");
                return tokenMessage != null && tokenMessage.User.IdUser > 0 ? tokenMessage.Token : string.Empty;
            }
            set { }
        }

        public BlogEngineService(IConfiguration configuration, HttpClient httpClient, IHttpContextAccessor httpContextAccessor)
        {
            _Configuration = configuration;
            _HttpClient = httpClient;
            _baseUrl = this._Configuration.GetSection("BlogEngineAPI").GetValue<string>("BasePath");
            _httpContextAccessor = httpContextAccessor;
        }

        public BlogPost GetPostById(long idPost)
        {
            var url = $"{_baseUrl}/BlogEngine/GetPostById/{idPost}";

            var task = _HttpClient.GetAsync(url);
            var result = task.GetAwaiter().GetResult();

            BlogPost post = JsonSerializer.DeserializeAsync<BlogPost>(result.Content.ReadAsStreamAsync().Result, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }).Result;
            return post;
        }

        public List<BlogPost> GetAllPost()
        {
            var url = $"{_baseUrl}/BlogEngine/GetAllPost";

            var task = _HttpClient.GetAsync(url);
            var result = task.GetAwaiter().GetResult();

            List<BlogPost> listPosts = JsonSerializer.DeserializeAsync<List<BlogPost>>(result.Content.ReadAsStreamAsync().Result, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }).Result;
            return listPosts;
        }

        public List<BlogPost> GetPostsByAuthor(string author)
        {
            var url = $"{_baseUrl}/BlogEngine/GetPostsByAuthor/{author}";

            var task = _HttpClient.GetAsync(url);
            var result = task.GetAwaiter().GetResult();

            List<BlogPost> listPosts = JsonSerializer.DeserializeAsync<List<BlogPost>>(result.Content.ReadAsStreamAsync().Result, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }).Result;
            return listPosts;
        }

        public List<BlogPost> GetPostsByStatus(string status)
        {
            var url = $"{_baseUrl}/BlogEngine/GetPostsByStatus/{status}";

            var task = _HttpClient.GetAsync(url);
            var result = task.GetAwaiter().GetResult();

            List<BlogPost> listPosts = JsonSerializer.DeserializeAsync<List<BlogPost>>(result.Content.ReadAsStreamAsync().Result, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }).Result;
            return listPosts;
        }


        public PostDetail GetPostWithComments(long idPost)
        {
            var url = $"{_baseUrl}/BlogEngine/GetPostWithComments/{idPost}";

            var task = _HttpClient.GetAsync(url);
            var result = task.GetAwaiter().GetResult();

            PostDetail postDetail = JsonSerializer.DeserializeAsync<PostDetail>(result.Content.ReadAsStreamAsync().Result, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }).Result;
            return postDetail;
        }

        public async Task CreatePost(BlogPost post)
        {
            var url = $"{_baseUrl}/BlogEngine/CreatePost";
            var json = JsonSerializer.Serialize(post);
            _HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

            await _HttpClient.PostAsync(url, new StringContent(json, System.Text.Encoding.UTF8, "application/json"));

            return;
        }

        public async Task PublishPost(BlogPost post)
        {
            var url = $"{_baseUrl}/BlogEngine/PublishPost";
            var json = JsonSerializer.Serialize(post);
            _HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            await _HttpClient.PostAsync(url, new StringContent(json, System.Text.Encoding.UTF8, "application/json"));

            return;
        }

        public async Task RejectPost(BlogPost post)
        {
            var url = $"{_baseUrl}/BlogEngine/RejectPost";
            var json = JsonSerializer.Serialize(post);
            _HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            await _HttpClient.PostAsync(url, new StringContent(json, System.Text.Encoding.UTF8, "application/json"));

            return;
        }

        public async Task SubmitPost(BlogPost post)
        {
            var url = $"{_baseUrl}/BlogEngine/SubmitPost";
            var json = JsonSerializer.Serialize(post);
            _HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            await _HttpClient.PostAsync(url, new StringContent(json, System.Text.Encoding.UTF8, "application/json"));

            return;
        }

        public async Task UpdatePost(BlogPost post)
        {
            var url = $"{_baseUrl}/BlogEngine/UpdatePost";
            var json = JsonSerializer.Serialize(post);
            _HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            await _HttpClient.PostAsync(url, new StringContent(json, System.Text.Encoding.UTF8, "application/json"));

            return;
        }

        public async Task AddCommentPost(BlogComment commentPost)
        {
            var url = $"{_baseUrl}/BlogEngine/AddCommentPost";
            var json = JsonSerializer.Serialize(commentPost);
            _HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            await _HttpClient.PostAsync(url, new StringContent(json, System.Text.Encoding.UTF8, "application/json"));

            return;
        }
    }
}
