﻿using System.Linq.Expressions;
using Zemoga.BlogEngine.Web.Models;

namespace Zemoga.BlogEngine.Web.Services.Interfaces
{
    public interface IBlogEngineService
    {
        public BlogPost GetPostById(long idPost);
        List<BlogPost> GetAllPost();
        List<BlogPost> GetPostsByAuthor(string author);
        List<BlogPost> GetPostsByStatus(string status);
        PostDetail GetPostWithComments(long idPost);
        Task CreatePost(BlogPost post);
        Task UpdatePost(BlogPost post);
        Task SubmitPost(BlogPost post);
        Task PublishPost(BlogPost post);
        Task RejectPost(BlogPost post);
        Task AddCommentPost(BlogComment commentPost);
    }
}
