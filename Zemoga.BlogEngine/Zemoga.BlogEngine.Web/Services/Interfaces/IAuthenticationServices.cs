﻿using Zemoga.BlogEngine.Web.Models;

namespace Zemoga.BlogEngine.Web.Services.Interfaces
{
    public interface IAuthenticationServices
    {
        Task<TokenMessage> Login(LoginModelVM loginModelVM);
    }
}
