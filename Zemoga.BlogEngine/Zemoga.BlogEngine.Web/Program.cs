using Zemoga.BlogEngine.Web.Services.Implementations;
using Zemoga.BlogEngine.Web.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<IBlogEngineService, BlogEngineService>();
builder.Services.AddScoped<IAuthenticationServices, AuthenticationServices>();

// Add services to the container.
builder.Services.AddControllersWithViews();
var httpClient = new HttpClient();
httpClient.DefaultRequestHeaders.Accept.Clear();
builder.Services.AddSingleton<HttpClient>(httpClient);

builder.Services.AddDistributedMemoryCache();

builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(20);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();
app.UseSession();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
