﻿namespace Zemoga.BlogEngine.Web.Models
{
    public class TokenMessage
    {
        public User User { get; set; } = new User();
        public string Token { get; set; } = string.Empty;
    }
}
