﻿namespace Zemoga.BlogEngine.Web.Models
{
    public class BlogPost
    {
		public long IdPost { get; set; }
		public string Title { get; set; }
		public string Content { get; set; }
		public DateTime PublishedDate { get; set; }
		public string Author { get; set; }
		public string Status { get; set; }
	}
}
