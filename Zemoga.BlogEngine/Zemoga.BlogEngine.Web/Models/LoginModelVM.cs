﻿using System.ComponentModel.DataAnnotations;

namespace Zemoga.BlogEngine.Web.Models
{
    public class LoginModelVM
    {
        [Required(ErrorMessage = "* Required")]
        [Display(Name = "User Name")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "* Required")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
