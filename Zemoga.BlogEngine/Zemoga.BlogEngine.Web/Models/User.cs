﻿namespace Zemoga.BlogEngine.Web.Models
{
    public class User
    {
        public int IdUser { get; set; }
        public string Name { get; set; } = string.Empty;
        public string UserName { get; set; } = String.Empty;
        public string Password { get; set; } = String.Empty;
        public string Role { get; set; } = String.Empty;
    }
}
