﻿namespace Zemoga.BlogEngine.Web.Models
{
    public class BlogComment
    {
        public long IdComment { get; set; }
        public long IdPost { get; set; }
        public string Comment { get; set; }
        public string Author { get; set; }
        public DateTime DateComment { get; set; }
    }
}
