﻿namespace Zemoga.BlogEngine.Web.Models
{
    public class PostDetail
    {
        public BlogPost BlogPost { get; set; }
        public List<BlogComment> BlogComments { get; set; } = new List<BlogComment>();
    }
}
