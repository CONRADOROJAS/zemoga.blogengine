﻿namespace Zemoga.BlogEngine.Web.Models
{
    public class Role
    {
        public const string Public = "Public";
        public const string Writer = "Writer";
        public const string Editor = "Editor";
    }
}
