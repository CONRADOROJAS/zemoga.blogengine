﻿namespace Zemoga.BlogEngine.Web.Models
{
    public class StatusPost
    {
        public const string Created = "Created";
        public const string Published = "Published";
        public const string Rejected = "Rejected";
        public const string PendingAproval = "PendingAproval";
    }
}
