﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Zemoga.BlogEngine.Business.Interfaces;
using Zemoga.BlogEngine.Core.EF;
using Zemoga.BlogEngine.Core.Models;
using Zemoga.BlogEngine.Infraestructure.Interfaces;

namespace Zemoga.BlogEngine.Business.Implementations
{
    public class BlogEngineService : IBlogEngineService
    {
        private readonly IRepositoryBlogEngine<BlogPost> _RepositoryPost;
        private readonly IRepositoryBlogEngine<BlogComment> _RepositoryBlogComment;
        public BlogEngineService(IRepositoryBlogEngine<BlogPost> repositoryPost, IRepositoryBlogEngine<BlogComment> repositoryBlogComment)
        {
            _RepositoryPost = repositoryPost;
            _RepositoryBlogComment = repositoryBlogComment;
        }

        public BlogPost? GetPost(Expression<Func<BlogPost, bool>> predicate)
        {
            return _RepositoryPost.Get(predicate).FirstOrDefault();
        }

        public List<BlogPost> GetPosts(Expression<Func<BlogPost, bool>> predicate)
        {
            return _RepositoryPost.Get(predicate).ToList();
        }

        public List<BlogPost> GetAllPost()
        {
            return _RepositoryPost.GetAll().ToList();
        }


        public PostDetail GetPostWithComments(long idPost)
        {
            var postDetail = new PostDetail();
            var post = _RepositoryPost.Get(x => x.IdPost == idPost).FirstOrDefault();
            postDetail.BlogPost = post ?? new BlogPost();
            postDetail.BlogComments = _RepositoryBlogComment.Get(x => x.IdPost == idPost).ToList();

            return postDetail;
        }


        public BlogPost CreatePost(BlogPost post)
        {
            return _RepositoryPost.Insert(post);
        }

        public BlogPost UpdatePost(BlogPost post)
        {
            return _RepositoryPost.Update(post);
        }

        public BlogPost SubmitPost(BlogPost post)
        {
            post.Status = "PendingAproval";
            return _RepositoryPost.Update(post);
        }

        public BlogPost PublishPost(BlogPost post)
        {
            post.Status = "Published";
            return _RepositoryPost.Update(post);
        }

        public BlogPost RejectPost(BlogPost post)
        {
            post.Status = "Rejected";
            return _RepositoryPost.Update(post);
        }

        public BlogComment AddCommentPost(BlogComment commentPost)
        {
            return _RepositoryBlogComment.Insert(commentPost);
        }

        public List<BlogComment> GetComments(Expression<Func<BlogComment, bool>> predicate)
        {
            return _RepositoryBlogComment.Get(predicate).ToList();
        }
    }
}
