﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Zemoga.BlogEngine.Core.EF;
using Zemoga.BlogEngine.Core.Models;

namespace Zemoga.BlogEngine.Business.Interfaces
{
    public interface IBlogEngineService
    {
        BlogPost? GetPost(Expression<Func<BlogPost, bool>> predicate);
        List<BlogPost> GetPosts(Expression<Func<BlogPost, bool>> predicate);
        List<BlogPost> GetAllPost();
        PostDetail GetPostWithComments(long idPost);
        BlogPost CreatePost(BlogPost post);
        BlogPost UpdatePost(BlogPost post);
        BlogPost SubmitPost(BlogPost post);
        BlogPost PublishPost(BlogPost post);
        BlogPost RejectPost(BlogPost post);
        BlogComment AddCommentPost(BlogComment commentPost);
        List<BlogComment> GetComments(Expression<Func<BlogComment, bool>> predicate);
    }
}
