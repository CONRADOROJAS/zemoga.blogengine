﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zemoga.BlogEngine.Core.EF;

namespace Zemoga.BlogEngine.Core.Models
{
    public  class PostDetail
    {
        public BlogPost BlogPost { get; set; }
        public List<BlogComment> BlogComments { get; set; } = new List<BlogComment>();   
    }
}
