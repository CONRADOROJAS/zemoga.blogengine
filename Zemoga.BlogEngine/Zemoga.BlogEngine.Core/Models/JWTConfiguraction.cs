﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zemoga.BlogEngine.Core.Models
{
    public class JWTConfiguraction
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
    }
}
