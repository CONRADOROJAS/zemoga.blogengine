﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zemoga.BlogEngine.Core.Models
{
    public class TokenMessage
    {
        public User User { get; set; } = new User();
        public string Token { get; set; } = string.Empty;
    }
}
