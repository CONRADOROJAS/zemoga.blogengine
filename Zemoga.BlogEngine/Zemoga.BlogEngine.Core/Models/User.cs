﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zemoga.BlogEngine.Core.Models
{
    public class User
    {
        public int IdUser { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }

    }

    public class UserData
    {
        public List<User>  Users { get; set; }

        public UserData()
        {
            Users = new List<User>() {
                new User() { IdUser=1, Name = "public", UserName = "public",  Password= "1234.", Role = "Public"},
                new User() { IdUser=1, Name = "Conrado Rojas", UserName = "conrado",  Password= "1234.", Role = "Writer"},
                new User() { IdUser = 1, Name = "Jesus Vargas", UserName = "jesus", Password = "1234.", Role = "Editor" },
                new User() { IdUser = 1, Name = "Brayan Rojas", UserName = "brayan", Password = "1234.", Role = "Writer" },
                new User() { IdUser = 1, Name = "Ajelandro Carvajal", UserName = "ajelandro", Password = "1234.", Role = "Editor" }
            };   
        }
    }
}
