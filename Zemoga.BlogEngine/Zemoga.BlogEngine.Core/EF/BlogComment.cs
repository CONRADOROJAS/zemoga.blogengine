﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zemoga.BlogEngine.Core.EF
{
    [Table("BlogComment", Schema = "dbo")]
    public class BlogComment
    {
        [Key]
        public long IdComment { get; set; }
        public long IdPost { get; set; }
        public string Comment { get; set; }
        public string Author { get; set; }
        public DateTime DateComment { get; set; }
    }
}
