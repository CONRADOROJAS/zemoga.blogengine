﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zemoga.BlogEngine.Core.Enums
{
    public enum Role
    {
        Public,
        Writer,
        Editor
    }
}
