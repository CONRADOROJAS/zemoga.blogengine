﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zemoga.BlogEngine.Business.Implementations;
using Zemoga.BlogEngine.Business.Interfaces;
using Zemoga.BlogEngine.Infraestructure.Interfaces;
using Zemoga.BlogEngine.Infraestructure.Implementations;
using Microsoft.Extensions.DependencyInjection;
using Zemoga.BlogEngine.Infraestructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Zemoga.BlogEngine.Core.Enums;
using Microsoft.Extensions.Configuration;

namespace Zemoga.BlogEngine.Root
{
    public class CompositionRoot
    {
        public CompositionRoot()
        {
        }
        public static void InjectDependencies(IServiceCollection services)
        {
            services.AddDbContext<DatabaseContext>(opts => opts.UseSqlServer("name=ConnectionStrings:default"));

            services.AddScoped(typeof(IRepositoryBlogEngine<>), typeof(RepositoryBlogEngine<>));

            services.AddScoped<IBlogEngineService, BlogEngineService>();
        }

        public static void SetupAuthorization(IServiceCollection services, ConfigurationManager configuration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
               .AddJwtBearer(options =>
               {
                   options.TokenValidationParameters = new TokenValidationParameters
                   {
                       ValidateIssuerSigningKey = true,
                       IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8
                           .GetBytes(configuration.GetSection("JWTSettings:SecretKey").Value)),
                       ValidateIssuer = false,
                       ValidateAudience = false
                   };
               });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("Public", policy =>
                   policy.RequireRole(Role.Public.ToString(), Role.Writer.ToString(), Role.Editor.ToString()));
                options.AddPolicy("Writer", policy =>
                   policy.RequireRole(Role.Writer.ToString()));
                options.AddPolicy("Editor", policy =>
                   policy.RequireRole(Role.Editor.ToString()));
            });
        }

    }

}
