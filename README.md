# Zemoga.BlogEngine



##  Software Prerequisites

Visual Studio 2022
.Net Core Framework 6.0 

##  Steps to build the app

- Download the repository based in the branch "main"

- Open the solution in Visual Studio 2022

- Open SQL server Management Studio

- Create a new database "BlogEngineDB"

Execute the following script:

    CREATE TABLE [dbo].[BlogPost]
    (
        [IdPost] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,
        [Title] [varchar](255) NOT NULL,
        [Content] [varchar](max) NOT NULL,
        [PublishedDate] [date] NOT NULL,
        [Author] [varchar](255) NOT NULL,
        [Status] [varchar](50) NOT NULL
    )

    CREATE TABLE [dbo].[BlogComment]
    (
        [IdComment] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,
        [IdPost] [bigint] NOT NULL,
        [Comment] [varchar](max) NOT NULL,
        [Author] [varchar](255) NOT NULL,
        [DateComment] [date] NOT NULL
    )
    GO


ALTER TABLE [dbo].[BlogComment]  WITH CHECK ADD  CONSTRAINT [FK_MessagePost_Post] FOREIGN KEY([IdPost])
REFERENCES [dbo].[BlogPost] ([IdPost])
GO


- In the project "Zemoga.BlogEngine.API" change the settings "ConnectionStrings" in the app appsettings
- In the project "Zemoga.BlogEngine.Web" change the settings "BasePath" in the app appsettings with the value "http://localhost:7279/"

- In the solution properties select the option Multiple startup project and select to start the projects API and Web simultaneously.

## Sample credentials for different types of user
Role:  Writer 
user: conrado
password: 1234.

Role: Editor
user: jesus
password: 1234.






